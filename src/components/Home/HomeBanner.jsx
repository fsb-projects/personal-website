import React from 'react'
import PropTypes from 'prop-types'


const HomeBanner=({title,subtitle,description,button,image})=>{

    return (
        <section id="banner">
        <div className="content">
          <header>
            <h1 style={{whiteSpace: "pre"}}>{title}</h1>
            <p>{subtitle}</p>
          </header>
          <p>{description}</p>
          <ul className="actions">
            <li><a href={button.link} className="button big" download>{button.text}</a></li>
          </ul>
        </div>
        <span className="image object">
          <img src={image.src} alt={image.alt} style={{maxWidth:"380px",margin:"0 auto"}}/>
        </span>
      </section>
    )
}
HomeBanner.protoTypes={
  title:PropTypes.string,
  subtitle:PropTypes.string,
  description:PropTypes.string,
  button:PropTypes.string,
  image:PropTypes.string,
  
}
export default HomeBanner