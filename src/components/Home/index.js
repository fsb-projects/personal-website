import React from 'react'
import HomeBanner from './HomeBanner'
import PostsPreview from './PostsPreview'
import ContentOverview from './ContentOverview'
const Home=({homeBannerProps,projects,skills})=>{

    return (
        <>
          <HomeBanner {...homeBannerProps}/>
          <ContentOverview skills={skills}/>
          <PostsPreview {...projects}/>      
        </>
    )
}
export default Home