import React from 'react'
import PropTypes from 'prop-types'

const styles={
   buttonAction:{
    display:"flex",
    justifyContent:"space-between"
  },
   article:{
    display:"flex",
    flexDirection:"column",
    justifyContent:"space-between"
  },
   sectionCss:{
    margin:"0 auto",
    maxWidth:"1200px"
  }

}
const PostPreview=({data,loading,error})=>{
    
    return (<section id="projects" >
        <header className="major">
          <h2>Projects</h2>
        </header>
        <p>All public projects are available on <a href="https://gitlab.com/fsb-projects/react">FSBalbuena`s gitlab react projects folder</a></p>
        <div className="posts" style={styles.sectionCss}>
        {data.length?data.slice(0,3).map(project=>(
          <article style={styles.article} key={project.id}>
          <a href={project.sourceUrl} target="_blank" className="image">
          <img src={project.image} alt="Project Picture"/> 
          </a>
          <h3>{project.title}</h3>
          <p>{project.description}</p>
          <ul className="actions" style={styles.buttonAction}>
            <li><a href={project.sourceUrl} target="_blank" className="button">Source</a></li>
            <li>{project.demoUrl?<a href={project.demoUrl} target="_blank" className="button primary">View</a>:null}</li>
          </ul>
        </article>
        )):null}
        </div>
      </section>
      )
}
PostPreview.propTypes={
  data:PropTypes.arrayOf(
    PropTypes.shape({
      id:PropTypes.string,
      image:PropTypes.string,
      title:PropTypes.string,
      description:PropTypes.string,
      content:PropTypes.string,
      publish:PropTypes.bool,
      sourceUrl:PropTypes.string,
      demoUrl:PropTypes.string,
      tags:PropTypes.arrayOf(
        PropTypes.shape({
          id:PropTypes.number,
          name:PropTypes.string
        })
      )
    })
    ),
    loading:PropTypes.bool,
    error:PropTypes.object

}

export default PostPreview