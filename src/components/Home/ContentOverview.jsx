import React from 'react'
import PropTypes from 'prop-types'
const ContentOverview=({skills})=>{
    return (
        <section>
        <header className="major">
          <h2>Skills</h2>
        </header>
        <div className="features">
          {skills.length?skills.map(skill=>(
              <article key={skill.title}>
              <span className={skill.iconClassName}></span>
              <div className="content">
                <h3>{skill.title}</h3>
                <p>{skill.description} </p>
              </div>
            </article>
            )):null}
        </div>
      </section>
    )
}
ContentOverview.propTypes={
  skills:PropTypes.arrayOf(
    PropTypes.shape({
      iconClassName:PropTypes.string,
      title:PropTypes.string,
      description:PropTypes.string
    })
  )
}

export default ContentOverview