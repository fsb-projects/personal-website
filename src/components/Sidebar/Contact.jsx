import React from 'react'
import Section from '../Section'
const Contact=()=>{

    return (
    <Section title={"Get in touch!"} titleSize={"2"}>
        <p>Feel free to reach me out for concerns about my projects, blog`s post, or job offers .</p>
        <ul className="contact">
            <li className="icon solid fa-envelope"><a href="#">fsbalbuena@gmail.com</a></li>
            <li className="icon solid fa-home">J D Peron 1500<br />
            CABA, Argentina</li>
        </ul>
    </Section>
)
}

export default Contact