import React from 'react'

const MiniPost=()=>{

    return (<section>
        <header className="major">
            <h2>Linkedin Posts</h2>
        </header>
        <div className="mini-posts">
            <article>
                <a href="https://www.linkedin.com/pulse/como-evitar-que-un-robot-nos-remplace-federico-sebastian-balbuena/" className="image"><img src="images/how-to-avoid-article.jpg" alt="" /></a>
                <p>¿Como evitar que un robot nos reemplace?</p>
            </article>
            <article>
                <a href="https://www.linkedin.com/pulse/donde-se-dirige-la-tecnolog%C3%ADa-federico-sebastian-balbuena/" className="image"><img src="images/where-we-go-article.jpg" alt="" /></a>
                <p>¿A donde se dirige la tecnologia?</p>
            </article>
            <article>
                <a href="https://www.linkedin.com/pulse/la-habitaci%C3%B3n-del-tiempo-de-dragonball-es-real-en-balbuena/" className="image"><img src="images/bootcamp-article.jpg" alt="" /></a>
                <p>La "habitacion del tiempo" es real.</p>
            </article>
        </div>
        <ul className="actions">
            <li><a href="https://www.linkedin.com/in/fsbalbuena/" className="button">Linkedin</a></li>
        </ul>
    </section>)
}

export default MiniPost