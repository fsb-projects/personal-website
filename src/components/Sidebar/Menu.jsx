import React from 'react'
const Menu=()=>{
    const handleScroll=(id)=>{
        var elmnt = document.getElementById(id);
        elmnt.scrollIntoView();
    }
    return (
        <nav id="menu">
        <header className="major">
            <h2>Menu</h2>
        </header>
        <ul>
            <li><span onClick={()=>handleScroll("banner")} >Home</span></li>
            <li><a href="React-developer-Federico-Balbuena.pdf" download>resume</a></li>
            <li><span onClick={()=>handleScroll("projects")} >Projects</span></li>
        </ul>
    </nav>
    )
}

export default Menu