import React,{useState} from 'react'

const Component=()=>{
	const [value,setValue]=useState("")
	const handleChange=(e)=>setValue(e.target.value)
	const handleSubmit=(e)=>{
		e.preventDefault()
		e.stopPropagation()
		console.log(`Submit! with ${value}`)
	}
    return (
        <section id="search" className="alt">
			<form onSubmit={handleSubmit}>
				<input type="text" value={value} onChange={handleChange} placeholder="Search" />
			</form>
		</section>
    )
}

export default Component