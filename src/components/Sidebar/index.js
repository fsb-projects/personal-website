import React, {useState} from 'react'
import Search from './Search'
import Menu from './Menu'
import MiniPost from './MiniPost'
import Contact from './Contact'
import Footer from './Footer'
const Sidebar =()=>{
    const [collapse,setCollapse]=useState(true)

    const handleClick=(e)=>{
        e.preventDefault()
        e.stopPropagation()
        setCollapse(!collapse)
    }
    const handleBlur=()=>setCollapse(true)
    return (
        <div id="sidebar" className={collapse?"inactive":""} onBlur={handleBlur}>
			<div className="inner">
				<Search />
				<Menu />
                <MiniPost/>
                <Contact/>
                <Footer/>
			</div>
            <a href="#" className="toggle" onClick={handleClick}>Toggle</a>
		</div>
    )
}
export default Sidebar