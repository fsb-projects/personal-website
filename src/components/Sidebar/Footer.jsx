import React from 'react'

const Footer=()=>{

    return (
    <footer id="footer">
        <p className="copyright">&copy; Code with love by FSBalbuena, Design: <a href="https://html5up.net">HTML5 UP</a>.</p>
    </footer>)
}

export default Footer