import React from 'react'
import {Link} from 'react-router-dom'


const icons=[
    {className:"fa-linkedin",
    link:"https://www.linkedin.com/in/fsbalbuena/",
    label:"Linkedin"},
    {className:"fa-github",
    link:"https://github.com/FSBalbuena",
    label:"Github"},
    {className:"fa-instagram",
    link:"https://www.instagram.com/fsbalbuena/",
    label:"Instagram"}
]

const BrandIcon=({label,className,link})=>(
    <li>
        <a href={link} target="_blank" className={`icon brands ${className}`}>
        <span className="label">{label}</span>
        </a>
    </li>
                                                        
)

const SocialMediaIcons=({icons})=>{
    return (
        <ul className="icons">
            {icons.map(icon=>(
                <BrandIcon key={icon.label} {...icon}/>
            ))}
        </ul>
    )
}
const HomeHeader=()=>{

    return (
        <header id="header">
          <Link className="logo" to="/" >
          <strong>FSBalbuena</strong>
          -From Corrientes to the world</Link>
          <SocialMediaIcons icons={icons}/>
        </header>
    )
}

export default HomeHeader