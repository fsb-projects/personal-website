import React from 'react'
import PropTypes from 'prop-types'


const Section=({title,children,titleSize="2"})=>(
    <section>
        <header className="major">
        {titleSize==="2"?<h2>{title}</h2>:
        titleSize==="3"?<h3>{title}</h3>:
        <h4>{title}</h4>}
        </header>
        {children}
    </section>
)
Section.propTypes={
    title:PropTypes.string,
    children:PropTypes.array,
    titleSize:PropTypes.string,
}
export default Section

