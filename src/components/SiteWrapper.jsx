import React from 'react'

const SiteWrapper=({children})=>{

    return (
        <div id="main">
              <div className="inner">
                {children}
              </div>
            </div>
    )
}
export default SiteWrapper