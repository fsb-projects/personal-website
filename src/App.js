import React,{useEffect} from 'react';
import Sidebar from './components/Sidebar'
import SiteWrapper from './components/SiteWrapper'
import Header from './components/Header'
import HomeContainer from './containers/HomeContainer'
import {Route,Switch,Redirect} from 'react-router-dom'
import {setUiWidth} from './redux/actions/ui-actions'
import { connect } from 'react-redux';

const App= ({setUiWidth})=>{
    setUiWidth(window.innerWidth)
    useEffect(
        ()=>{
            const listener=(e)=>setUiWidth(e.target.innerWidth)
            window.addEventListener('resize',listener)
            return ()=>window.removeEventListener('resize',listener)
        }
    )
    return (
        <>
            <SiteWrapper>
            <Header/>
            <Switch>
                <Route path="/home" exact component={HomeContainer}/>
                <Redirect from="/" to="/home"/>
            </Switch>
            </SiteWrapper>
            <Sidebar/>
        </>
    )
}

const mapDispatchToProps=(dispatch,ownProps)=>({
    setUiWidth:(width)=>dispatch(setUiWidth(width))
})
export default connect(null,mapDispatchToProps)(App)

