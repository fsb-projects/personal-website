export default [
    {
        concept:"University",
        data:[
                {
                 title:"Degree in system`s analysis",
                 startDate:"Jul 2016",
                 endDate:"",
                 institution:"UBA",
                 state:"in process"
                },
                {
                title:"Industrial engineering",
                 startDate:"Jan 2012",
                 endDate:"Jun 2016",
                 institution:"UBA",
                 state:"incomplete"
                }
        ]
    },
    {
        concept:"Course",
        data:[
            {   title:"Fullstack web development (600hs)",
                startDate:"Jan 2018",
                endDate:"Apr 2018",
                institution:"Plataforma 5 coding bootcamp",
                state:"finished"
            },
            {   title:"Introduction to web development",
                startDate:"Jan 2017",
                endDate:"Mar 2017",
                institution:"Plataforma 5 coding bootcamp",
                state:"finished"
            },
            {   title:"Traditional and online marketing",
                startDate:"Sep 2011",
                endDate:"Nov 2011",
                institution:"CETAE",
                state:"finished"
            }
        ]
    },
    {
        concept:"Seminar",
        data:[
            {   title:"International contracts in foreign trade",
                startDate:"Ago 2016",
                endDate:"Ago 2016",
                institution:"CAC",
                state:"finished"
            },
            {   title:"Change Management",
                 startDate:"Jun 2016",
                 endDate:"Jun 2016",
                 institution:"CAC",
                 state:"finished"
             },
            {   title:"Income & taxes",
                 startDate:"Jan 2016",
                 endDate:"Jan 2016",
                 institution:"CAPACITARTE",
                 state:"finished"
             }
        ]
    }
]