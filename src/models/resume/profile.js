export default {
    imageUrl:"/images/fsbalbuena.jpg",
    name:"Federico Balbuena",
    title:"Fullstack Developer",
    description:"Full Stack React & Node.js Developer, teacher , innovation strategy and CX evangelist, with an entrepreneurial spirit.",
    personalInfo:[
        {concept:"age",
        value:"28"
        },
        {concept:"nationality",
        value:"Argentinian"
        },
        {concept:"adress",
        value:"Tte Gral J D Peron 1570, Bs As (Arg)"
        },
        {concept:"email",
        value:"fsbalbuena@gmail.com"
        },
        {concept:"tel",
        value:"+54-911-3101-7887"
        },
        {concept:"website",
        value:"//www.fsbalbuena.com"
        },
        {concept:"skype",
        value:"fsbalbuena"
        },
        {concept:"linkedin",
        value:"fsbalbuena"
        },   
        {concept:"github",
        value:"fsbalbuena"
        }

    ]
}
    