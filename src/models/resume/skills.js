export default [
    {
        concept:"Programing",
        data:[
            "JavaScript",
            "Node.JS",
            "React",
            "Redux",
            "Helmet.js",
            "MongoDB",
            "Mongoose",
            "PostgreSQL",
            "Sequelize",
            "Python",
            "SQL",
            "Socket.io",
            "HTML",
            "CSS",
            "flexbox",
            "grid",
            "Bootstrap",
            "jQuery",
            "Office",
            "JD Edwards",
            "Servicios web de RESTful",
            "GitHub",
            "OracleServiceCloud",
            "OracleSocialCloud",
        ]
    },
    {
        concept:"Management",
        data:[
            "Leadership",
            "Customer Experience",
            "Change Management",
            "Entrepreneurship"
        ]
    },
    {
        concept:"Innovation",
        data:[
            "Innovation Strategy",
             "Design Thinking",
             "Persona design",
             "Three box Solution"
        ]
    }
]
