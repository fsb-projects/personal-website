import profile from "./profile"
import education from "./education"
import languages from "./languages"
import experience from "./experience"
import skills from "./skills"

export default {
        languages,
        education,
        experience,
        skills,
        profile   
}

