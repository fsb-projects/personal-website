export default [
        {
            name:"English",
            level:"Advanced"
        },
        {
            name:"Portugues",
            level:"Intermediate"
        },
        {
            name:"Spanish",
            level:"Native"
        },
        {
            name:"Chinese",
            level:"Basic"
        }
]