export default [
    {
        startDate:"Feb 2019",
        endDate:"",
        company:"Plataforma5 Coding Bootcamp",
        position:"Javascript - EcmaScript 6 Teacher",
        description:"Teach html - css - javaScript - ecmaScript 6 - Bootstrap 4 - jQuery (intermediate level)"
    },
    {
        startDate:"Sep 2018",
        endDate:"Feb 2019",
        company:"Innovegy",
        position:"Fullstack React Developer",
        description:"Freelance development on an internal project that was suspended due to lack of budget."
    },
    {
        startDate:"Jan 2018",
        endDate:"Sep 2018",
        company:"Ayi & Asociados",
        position:"Innovation Leader",
        description:"In charge of the innovation Strategy methodologies and POC developments on React and Node.js for clients.\nMy leadership was mainly focused on helping team members to organize priorities and design solutions based on:\n* Artificial Inteligence / Machine Learning\n* Blockchain\n* Internet of Things"
    },
    {
        startDate:"Jun 2017",
        endDate:"Jan 2018",
        company:"Ayi & Asociados",
        position:"CX Speciality Leader",
        description:"Manage Oracle Saas products, from de CX cloud module, set and align the software with business requirements and came up with creative integrations between oracle`s APIs and our custom Node.js backend solutions."
    }
]