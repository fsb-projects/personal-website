export default [
    {
        id:11,
        image:"images/country-app.png",
        title:"Country App",
        publish:true,
        description:"Fetch data from a GraphQL DB, using Apollo client on the front",
        tags:[
                {id:22,
                name:"react"},
                {id:23,
                name:"webpack"},
                {id:24,
                name:"apollo-client"},
                {id:25,
                name:"material-ui"}
            ],
        sourceUrl:"https://gitlab.com/fsb-projects/react/countryapp",
        demoUrl:"https://country-app-challenge.netlify.com" 
    },
    {
        id:12,
        image:"images/code-project.svg",
        title:"Rock it Baby",
        publish:true,
        description:"Fullstack spotify clone",
        tags:[
                {id:22,
                name:"react"},
                {id:26,
                name:"nodejs"},
                {id:27,
                name:"postgreSQL"},
                {id:28,
                name:"sequelize"},
                {id:23,
                name:"webpack"},
                {id:25,
                name:"material-ui"}
                
            ],
        sourceUrl:"https://gitlab.com/fsb-projects/react/fullstack-react-projects/rockitbaby",
        demoUrl:"" 
    },
    {
        id:13,
        image:"images/blog-mvp.png",
        title:"Blog MVP",
        publish:true,
        description:"Fullstack Development for a blog mvp",
        tags:[
                {id:22,
                name:"react"},
                {id:23,
                name:"webpack"},
                {id:26,
                name:"nodejs"},
                {id:25,
                name:"material-ui"}
            ],
        sourceUrl:"https://gitlab.com/fsb-projects/react/blog-mvp-challenge",
        demoUrl:"" 
    },
    {
        id:14,
        image:"images/test-project.png",
        title:"React Memory Test",
        publish:true,
        description:"Improve your memory with this game, you must remember all the words displayed on your level time",
        tags:[
                {id:22,
                name:"react"}
            ],
        sourceUrl:"https://gitlab.com/fsb-projects/react/react-memory-test",
        demoUrl:"https://fsb-react-memory-test.netlify.com/" 
    },
    {
        id:15,
        image:"images/drum-project.png",
        title:"IT Drums",
        publish:true,
        description:"Create your code at the rythm of drums, a pure frontend game",
        tags:[
                {id:22,
                name:"react"},
            ],
        sourceUrl:"https://gitlab.com/fsb-projects/react/react-drum-band",
        demoUrl:"https://fsb-react-drum-band.netlify.com/" 
    },
    {
        id:16,
        image:"images/pomodoro-project.png",
        title:"Pomodoro Clon",
        publish:true,
        description:"Set your work and relax periods with pure react and css",
        tags:[
                {id:22,
                name:"react"}
            ],
        sourceUrl:"https://gitlab.com/fsb-projects/react/react-time-tracker",
        demoUrl:"https://fsb-react-time-tracker.netlify.com/" 
    },
    {
        id:17,
        image:"images/calculator-project.png",
        title:"Calculator",
        publish:true,
        description:"Master the art of not over think so much and began to calculate small operations on this tool",
        tags:[
                {id:22,
                name:"react"}
            ],
        sourceUrl:"https://gitlab.com/fsb-projects/react/react-calculator",
        demoUrl:"https://fsb-react-calculator.netlify.com/" 
    },
    {
        id:18,
        image:"images/md-project.png",
        title:"Markdown previewer",
        publish:true,
        description:"The blog post on this app are made with this, a great feature to write quick and good content",
        tags:[
                {id:22,
                name:"react"}
            ],
        sourceUrl:"https://gitlab.com/fsb-projects/react/react-md-preview",
        demoUrl:"https://fsb-react-md-preview.netlify.com/" 
    },
]