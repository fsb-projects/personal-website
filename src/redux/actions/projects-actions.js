import { 
    ON_FETCH_ALL_PROJECTS,
    SUCCESS_ON_FETCH_ALL_PROJECTS
 } from '../constants';
  import projects from '../../models/projects'

  // we export this actions becaouse are being used on other model-actions too
  const fetchAllProjects=()=>({
        type:ON_FETCH_ALL_PROJECTS
        })
  const setAllProjects=(data)=>({
        type:SUCCESS_ON_FETCH_ALL_PROJECTS,
        data
        })

export const fetchProjects=()=>dispatch=>{
              dispatch(fetchAllProjects())
              dispatch(setAllProjects(projects))
              /*
              return axios.get("/api/projects")
              .then(res=>res.data)
              .then(data=>dispatch(setAllProjects(data)))
              .catch(err=>dispatch(setAllProjectsError(err.response.data)))
              */
            }