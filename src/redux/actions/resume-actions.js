import { 
    ON_FETCH_RESUME,
    SUCCESS_ON_FETCH_RESUME,
 } from '../constants';
  import resume from '../../models/resume'
  // we export this actions becaouse are being used on other model-actions too
  const fetchingResume=()=>({
        type:ON_FETCH_RESUME
        })
  const setResume=(data)=>({
        type:SUCCESS_ON_FETCH_RESUME,
        data
        })

export const fetchResume=()=>dispatch=>{
              dispatch(fetchingResume())
              dispatch(setResume(resume))
              /*
              //this is how should be implemented
              
              return axios.get("/api/resume")
              .then(res=>res.data)
              .then(data=>dispatch(setResume(data)))
              .catch(err=>dispatch(setResumeError(err.response.data)))
              */
            }