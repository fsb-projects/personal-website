import { 
      UI_SET_WIDTH
 } from '../constants';
 
 
  const uiWidth=(data)=>({
        type:UI_SET_WIDTH,
        data
        })

      
export const setUiWidth=(width)=>dispatch=>{
              if (width<=600){
                    dispatch(uiWidth(true))
                  }else{
                        dispatch(uiWidth(false))
                  }
}