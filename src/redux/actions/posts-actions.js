import { 
    ON_FETCH_ALL_POSTS,
    SUCCESS_ON_FETCH_ALL_POSTS,
    ON_FETCH_CURRENT_POST,
    SUCCESS_ON_FETCH_CURRENT_POST,
    ERROR_ON_FETCH_CURRENT_POST,
 } from '../constants';
 
  import posts from '../../models/posts'
  
  const fetchAllPosts=()=>({
        type:ON_FETCH_ALL_POSTS
        })
  const setAllPosts=(data)=>({
        type:SUCCESS_ON_FETCH_ALL_POSTS,
        data
        })
  const fetchCurrentPost=()=>({
        type:ON_FETCH_CURRENT_POST
        })
  const setCurrentPost=(data)=>({
        type:SUCCESS_ON_FETCH_CURRENT_POST,
        data
        })
  const errorCurrentPost=(data)=>({
        type:ERROR_ON_FETCH_CURRENT_POST,
        data
        })
      
export const fetchPosts=()=>dispatch=>{
              dispatch(fetchAllPosts())
              dispatch(setAllPosts(posts))
              /*
              return axios.get("/api/posts")
              .then(res=>res.data)
              .then(data=>dispatch(setAllPosts(data)))
              .catch(err=>dispatch(setAllPostsError(err.response.data)))
              */
            }
export const fetchSinglePost=(id)=>dispatch=>{
              dispatch(fetchCurrentPost())
              const data=posts.filter(post=>post.id===id)[0]
              if(data){
                    dispatch(setCurrentPost(data))
              }else{
                    dispatch(errorCurrentPost({message:"Post not found"}))
              }
            }

export const clearCurrentPost=()=>dispatch=>{
            dispatch(setCurrentPost({}))
            }