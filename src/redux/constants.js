export const CONSTANT="CONSTANT"
//--------------------------------UI LOADING--------------------------
//---------LOADING TRUE AND ERROR FALSE
export const ON_CREATE_POST="ON_CREATE_POST"
export const ON_UPDATE_POST="ON_UPDATE_POST"
export const ON_DELETE_POST="ON_DELETE_POST"

export const ON_CREATE_PROJECT="ON_CREATE_PROJECT"
export const ON_UPDATE_PROJECT="ON_UPDATE_PROJECT"
export const ON_DELETE_PROJECT="ON_DELETE_PROJECT"

export const ON_CREATE_RESUME_SECTION="ON_CREATE_RESUME_SECTION"
export const ON_UPDATE_RESUME_SECTION="ON_UPDATE_RESUME_SECTION"
export const ON_DELETE_RESUME_SECTION="ON_DELETE_RESUME_SECTION"

export const UI_LOADING="UI_LOADING"
export const UI_STOP_LOADING="UI_STOP_LOADING"

//------------------------------UI ERROR-------------------------------
export const ERROR_ON_CREATE_POST="ERROR_ON_CREATE_POST"
export const ERROR_ON_UPDATE_POST="ERROR_ON_UPDATE_POST"
export const ERROR_ON_DELETE_POST="ERROR_ON_DELETE_POST"

export const ERROR_ON_CREATE_PROJECT="ERROR_ON_CREATE_PROJECT"
export const ERROR_ON_UPDATE_PROJECT="ERROR_ON_UPDATE_PROJECT"
export const ERROR_ON_DELETE_PROJECT="ERROR_ON_DELETE_PROJECT"

export const ERROR_ON_CREATE_RESUME_SECTION="ERROR_ON_CREATE_RESUME_SECTION"
export const ERROR_ON_UPDATE_RESUME_SECTION="ERROR_ON_UPDATE_RESUME_SECTION"
export const ERROR_ON_DELETE_RESUME_SECTION="ERROR_ON_DELETE_RESUME_SECTION"

export const UI_SET_ERRORS="UI_SET_ERRORS"
export const UI_CLEAR_ERRORS="UI_CLEAR_ERRORS"
//-------------------------------UI MOBILE----------------------------------
export const UI_SET_WIDTH="UI_SET_WIDTH"

//----------------------------------------------------------------------------------------------------------------------------------
//-----------------------------ALL POSTS--------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------

//-----------------------------ALL POSTS LOADING-------------------

export const ON_FETCH_ALL_POSTS="ON_FETCH_ALL_POSTS"

//-----------------------------ALL POSTS DATA----------------------

export const SUCCESS_ON_FETCH_ALL_POSTS="SUCCESS_ON_FETCH_ALL_POSTS"

//-----------------------------ALL POSTS ERROR---------------------

export const ERROR_ON_FETCH_ALL_POSTS="ERROR_ON_FETCH_ALL_POSTS"
export const CLEAR_ERROR_ALL_POSTS="CLEAR_ERROR_ALL_POSTS"

//----------------------------------------------------------------------------------------------------------------------------------
//------------------------------CURRENT POST----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------
//-----------------------------CURRENT POST LOADING----------------

export const ON_FETCH_CURRENT_POST="ON_FETCH_CURRENT_POST"

//-----------------------------CURRENT POST DATA-------------------

export const SUCCESS_ON_FETCH_CURRENT_POST="SUCCESS_ON_FETCH_CURRENT_POST"

//-----------------------------CURRENT POST ERROR------------------

export const ERROR_ON_FETCH_CURRENT_POST="ERROR_ON_FETCH_CURRENT_POST"
export const CLEAR_ERROR_CURRENT_POST="CLEAR_ERROR_CURRENT_POST"

//----------------------------------------------------------------------------------------------------------------------------------
//-----------------------------ALL POSTS AND CURRENT POST DATA----------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------
export const SUCCESS_ON_CREATE_POST="SUCCESS_ON_CREATE_POST"
export const SUCCESS_ON_UPDATE_POST="SUCCESS_ON_UPDATE_POST"
export const SUCCESS_ON_DELETE_POST="SUCCESS_ON_DELETE_POST"

//----------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------
//-----------------------------ALL PROJECTS-----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------

//-----------------------------ALL PROJECTS LOADING-------------------

export const ON_FETCH_ALL_PROJECTS="ON_FETCH_ALL_PROJECTS"

//-----------------------------ALL PROJECTS DATA----------------------

export const SUCCESS_ON_FETCH_ALL_PROJECTS="SUCCESS_ON_FETCH_ALL_PROJECTS"

//-----------------------------ALL PROJECTS ERROR---------------------

export const ERROR_ON_FETCH_ALL_PROJECTS="ERROR_ON_FETCH_ALL_PROJECTS"
export const CLEAR_ERROR_ALL_PROJECTS="CLEAR_ERROR_ALL_PROJECTS"

//----------------------------------------------------------------------------------------------------------------------------------
//------------------------------CURRENT PROJECT-------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------
//-----------------------------CURRENT PROJECT LOADING----------------

export const ON_FETCH_CURRENT_PROJECT="ON_FETCH_CURRENT_PROJECT"

//-----------------------------CURRENT PROJECT DATA-------------------

export const SUCCESS_ON_FETCH_CURRENT_PROJECT="SUCCESS_ON_FETCH_CURRENT_PROJECT"

//-----------------------------CURRENT PROJECT ERROR------------------

export const ERROR_ON_FETCH_CURRENT_PROJECT="ERROR_ON_FETCH_CURRENT_PROJECT"
export const CLEAR_ERROR_CURRENT_PROJECT="CLEAR_ERROR_CURRENT_PROJECT"

//----------------------------------------------------------------------------------------------------------------------------------
//-----------------------------ALL PROJECTS AND CURRENT PROJECT DATA----------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------
export const SUCCESS_ON_CREATE_PROJECT="SUCCESS_ON_CREATE_PROJECT"
export const SUCCESS_ON_UPDATE_PROJECT="SUCCESS_ON_UPDATE_PROJECT"
export const SUCCESS_ON_DELETE_PROJECT="SUCCESS_ON_DELETE_PROJECT"

//----------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------
//-----------------------------RESUME-----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------

//-----------------------------RESUME LOADING-------------------

export const ON_FETCH_RESUME="ON_FETCH_RESUME"

//-----------------------------RESUME DATA----------------------

export const SUCCESS_ON_FETCH_RESUME="SUCCESS_ON_FETCH_RESUME"
export const SUCCESS_ON_CREATE_RESUME_SECTION="SUCCESS_ON_CREATE_RESUME_SECTION"
export const SUCCESS_ON_UPDATE_RESUME_SECTION="SUCCESS_ON_UPDATE_RESUME_SECTION"
export const SUCCESS_ON_DELETE_RESUME_SECTION="SUCCESS_ON_DELETE_RESUME_SECTION"

//-----------------------------RESUME ERROR---------------------

export const ERROR_ON_FETCH_RESUME="ERROR_ON_FETCH_RESUME"
export const CLEAR_ERROR_RESUME="CLEAR_ERROR_RESUME"