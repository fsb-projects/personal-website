import {
    ON_FETCH_RESUME,
    SUCCESS_ON_FETCH_RESUME,
    ERROR_ON_FETCH_RESUME,
    CLEAR_ERROR_RESUME,
    SUCCESS_ON_CREATE_RESUME_SECTION,
    SUCCESS_ON_UPDATE_RESUME_SECTION,
    SUCCESS_ON_DELETE_RESUME_SECTION,
    } from '../constants';
    
const defaultState={
    data:{},
    loading:false,
    error:{}
}

export default (state=defaultState,action)=>{
    switch (action.type) {
        case ON_FETCH_RESUME:
            return Object.assign({},state,{loading:true,error:{}})
        case SUCCESS_ON_FETCH_RESUME:
        case SUCCESS_ON_CREATE_RESUME_SECTION:
        case SUCCESS_ON_DELETE_RESUME_SECTION:
        case SUCCESS_ON_UPDATE_RESUME_SECTION:
            return Object.assign({},state,defaultState,{data:action.data})
        case ERROR_ON_FETCH_RESUME:
            return Object.assign({},state,{loading:false,error:action.data})
        case CLEAR_ERROR_RESUME:
            return Object.assign({},state,{error:{}})
        default:
            return Object.assign({},state)
    } 
}