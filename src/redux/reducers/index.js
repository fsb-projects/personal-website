import {combineReducers} from 'redux'
import uiReducer from './ui-reducer'
import allPostsReducer from './all-posts-reducer'
import allProjectsReducer from './all-projects-reducer'
import currentPostReducer from './current-post-reducer'
import currentProjectReducer from './current-project-reducer'
import resumeReducer from './resume-reducer'

export default combineReducers({
    ui:uiReducer,
    projects:allProjectsReducer,
    currentProject:currentProjectReducer,
    posts:allPostsReducer,
    currentPost:currentPostReducer,
    resume:resumeReducer
})
