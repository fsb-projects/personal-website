import {
    ON_CREATE_POST,
    ON_UPDATE_POST,
    ON_DELETE_POST,
    ON_CREATE_PROJECT,
    ON_UPDATE_PROJECT,
    ON_DELETE_PROJECT,
    ON_CREATE_RESUME_SECTION,
    ON_UPDATE_RESUME_SECTION,
    ON_DELETE_RESUME_SECTION,
    ERROR_ON_CREATE_POST,
    ERROR_ON_UPDATE_POST,
    ERROR_ON_DELETE_POST,
    ERROR_ON_CREATE_PROJECT,
    ERROR_ON_UPDATE_PROJECT,
    ERROR_ON_DELETE_PROJECT,
    ERROR_ON_CREATE_RESUME_SECTION,
    ERROR_ON_UPDATE_RESUME_SECTION,
    ERROR_ON_DELETE_RESUME_SECTION,
    SUCCESS_ON_CREATE_POST,
    SUCCESS_ON_UPDATE_POST,
    SUCCESS_ON_DELETE_POST,
    SUCCESS_ON_CREATE_PROJECT,
    SUCCESS_ON_UPDATE_PROJECT,
    SUCCESS_ON_DELETE_PROJECT,
    SUCCESS_ON_CREATE_RESUME_SECTION,
    SUCCESS_ON_UPDATE_RESUME_SECTION,
    SUCCESS_ON_DELETE_RESUME_SECTION,
    UI_SET_ERRORS,
    UI_CLEAR_ERRORS,
    UI_LOADING,
    UI_STOP_LOADING,
    UI_SET_WIDTH
    } from '../constants';
    
const defaultState={
    mobile:false,
    loading:false,
    error:{}
}

export default (state=defaultState,action)=>{
    switch (action.type) {
        case ON_CREATE_POST:
        case ON_UPDATE_POST:
        case ON_DELETE_POST:
        case ON_CREATE_PROJECT:
        case ON_UPDATE_PROJECT:
        case ON_DELETE_PROJECT:
        case ON_CREATE_RESUME_SECTION:
        case ON_UPDATE_RESUME_SECTION:
        case ON_DELETE_RESUME_SECTION:
            return Object.assign({},state,{loading:true,error:{}});
        case ERROR_ON_CREATE_POST:
        case ERROR_ON_UPDATE_POST:
        case ERROR_ON_DELETE_POST:
        case ERROR_ON_CREATE_PROJECT:
        case ERROR_ON_UPDATE_PROJECT:
        case ERROR_ON_DELETE_PROJECT:
        case ERROR_ON_CREATE_RESUME_SECTION:
        case ERROR_ON_UPDATE_RESUME_SECTION:
        case ERROR_ON_DELETE_RESUME_SECTION:
            return Object.assign({},state,{loading:false,error:action.data});
        case SUCCESS_ON_CREATE_POST:
        case SUCCESS_ON_UPDATE_POST:
        case SUCCESS_ON_DELETE_POST:
        case SUCCESS_ON_CREATE_PROJECT:
        case SUCCESS_ON_UPDATE_PROJECT:
        case SUCCESS_ON_DELETE_PROJECT:
        case SUCCESS_ON_CREATE_RESUME_SECTION:
        case SUCCESS_ON_UPDATE_RESUME_SECTION:
        case SUCCESS_ON_DELETE_RESUME_SECTION:
            return Object.assign({},state,{loading:false,error:{}});
        case UI_SET_ERRORS:
            return Object.assign({},state,{error:action.data});
        case UI_CLEAR_ERRORS:
            return Object.assign({},state,{error:{}});
        case UI_LOADING:
            return Object.assign({},state,{loading:true});
        case UI_STOP_LOADING:
            return Object.assign({},state,{loading:false});
        case UI_SET_WIDTH:
            return Object.assign({},state,{mobile:action.data});
        default:
            return Object.assign({},state);
    } 
}