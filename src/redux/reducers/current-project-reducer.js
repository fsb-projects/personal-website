import {
    ON_FETCH_CURRENT_PROJECT,
    SUCCESS_ON_FETCH_CURRENT_PROJECT,
    ERROR_ON_FETCH_CURRENT_PROJECT,
    CLEAR_ERROR_CURRENT_PROJECT,
    SUCCESS_ON_CREATE_PROJECT,
    SUCCESS_ON_UPDATE_PROJECT,
    SUCCESS_ON_DELETE_PROJECT,
    } from '../constants';
    
const defaultState={
    data:{},
    loading:false,
    error:{}
}

export default (state=defaultState,action)=>{
    switch (action.type) {
        case ON_FETCH_CURRENT_PROJECT:
            return Object.assign({},state,{loading:true,error:{}})
        case SUCCESS_ON_FETCH_CURRENT_PROJECT:
        case SUCCESS_ON_CREATE_PROJECT:
        case SUCCESS_ON_UPDATE_PROJECT:
            return Object.assign({},state,defaultState,{data:action.data})
        case SUCCESS_ON_DELETE_PROJECT:
            return Object.assign({},state,defaultState)
        case ERROR_ON_FETCH_CURRENT_PROJECT:
            return Object.assign({},state,{loading:false,error:action.data})
        case CLEAR_ERROR_CURRENT_PROJECT:
            return Object.assign({},state,{error:{}})
        default:
            return Object.assign({},state)
    } 
}