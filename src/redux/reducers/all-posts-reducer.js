import {
    ON_FETCH_ALL_POSTS,
    SUCCESS_ON_FETCH_ALL_POSTS,
    ERROR_ON_FETCH_ALL_POSTS,
    CLEAR_ERROR_ALL_POSTS,
    SUCCESS_ON_CREATE_POST,
    SUCCESS_ON_UPDATE_POST,
    SUCCESS_ON_DELETE_POST,
    } from '../constants';
    
const defaultState={
    data:[],
    loading:false,
    error:{}
}

export default (state=defaultState,action)=>{
    switch (action.type) {
        case ON_FETCH_ALL_POSTS:
            return Object.assign({},state,{loading:true,error:{}})
        case SUCCESS_ON_FETCH_ALL_POSTS:
            return Object.assign({},state,defaultState,{data:action.data})
        case ERROR_ON_FETCH_ALL_POSTS:
            return Object.assign({},state,{loading:false,error:action.data})
        case CLEAR_ERROR_ALL_POSTS:
            return Object.assign({},state,{error:{}})
        case SUCCESS_ON_CREATE_POST:
            return Object.assign({},state,{data:[action.data,...state.data]})
        case SUCCESS_ON_DELETE_POST:
            return Object.assign({},state,{
                data:state.data
                .filter(post=>post.id!==action.data.id)
                })
        case SUCCESS_ON_UPDATE_POST:
            return Object.assign({},state,{
                data:state.data
                .map(post=>post.id===action.data.id?
                            action.data
                            :
                            post)
                })
        default:
            return Object.assign({},state)
    } 
}