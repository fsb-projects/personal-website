import {
    ON_FETCH_CURRENT_POST,
    SUCCESS_ON_FETCH_CURRENT_POST,
    ERROR_ON_FETCH_CURRENT_POST,
    CLEAR_ERROR_CURRENT_POST,
    SUCCESS_ON_CREATE_POST,
    SUCCESS_ON_UPDATE_POST,
    SUCCESS_ON_DELETE_POST,
    } from '../constants';
    
const defaultState={
    data:{},
    loading:false,
    error:{}
}

export default (state=defaultState,action)=>{
    switch (action.type) {
        case ON_FETCH_CURRENT_POST:
            return Object.assign({},state,{loading:true,error:{}})
        case SUCCESS_ON_FETCH_CURRENT_POST:
        case SUCCESS_ON_CREATE_POST:
        case SUCCESS_ON_UPDATE_POST:
            return Object.assign({},state,defaultState,{data:action.data})
        case SUCCESS_ON_DELETE_POST:
            return Object.assign({},state,defaultState)
        case ERROR_ON_FETCH_CURRENT_POST:
            return Object.assign({},state,{loading:false,error:action.data})
        case CLEAR_ERROR_CURRENT_POST:
            return Object.assign({},state,{error:{}})
        default:
            return Object.assign({},state)
    } 
}