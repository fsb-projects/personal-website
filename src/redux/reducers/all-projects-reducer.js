import {
    ON_FETCH_ALL_PROJECTS,
    SUCCESS_ON_FETCH_ALL_PROJECTS,
    ERROR_ON_FETCH_ALL_PROJECTS,
    CLEAR_ERROR_ALL_PROJECTS,
    SUCCESS_ON_CREATE_PROJECT,
    SUCCESS_ON_UPDATE_PROJECT,
    SUCCESS_ON_DELETE_PROJECT,
    } from '../constants';
    
const defaultState={
    data:[],
    loading:false,
    error:{}
}

export default (state=defaultState,action)=>{
    switch (action.type) {
        case ON_FETCH_ALL_PROJECTS:
            return Object.assign({},state,{loading:true,error:{}})
        case SUCCESS_ON_FETCH_ALL_PROJECTS:
            return Object.assign({},state,defaultState,{data:action.data})
        case ERROR_ON_FETCH_ALL_PROJECTS:
            return Object.assign({},state,{loading:false,error:action.data})
        case CLEAR_ERROR_ALL_PROJECTS:
            return Object.assign({},state,{error:{}})
        case SUCCESS_ON_CREATE_PROJECT:
            return Object.assign({},state,{data:[action.data,...state.data]})
        case SUCCESS_ON_DELETE_PROJECT:
            return Object.assign({},state,{
                data:state.data
                .filter(project=>project.id!==action.data.id)
                })
        case SUCCESS_ON_UPDATE_PROJECT:
            return Object.assign({},state,{
                data:state.data
                .map(project=>project.id===action.data.id?
                            action.data
                            :
                            project)
                })
        default:
            return Object.assign({},state)
    } 
}