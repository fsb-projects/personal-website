import React,{useEffect} from 'react'
import { connect } from 'react-redux';
import Home from '../components/Home'
import {fetchProjects} from '../redux/actions/projects-actions'

const HomeContainer=(props)=>{
    useEffect(
        ()=>{
            props.fetchProjects()
            window.scrollTo(0, 0)
        },[]
    )
    
    return <Home {...props}/>
}
const mapStateToProps=(state)=>({
    projects:state.projects,
    isMobile:state.ui.mobile,
    homeBannerProps:{
        title:"Hi, I’m Federico,\nFullstack Developer",
        subtitle:"NodeJs - ReactJs",
        description:"I write code since 2016, knowing all the steps from static sites, through templates, to SWA.",
        button:{text:"Resume",
                link:"React-developer-Federico-Balbuena.pdf"},
        image:{src:"images/fsbalbuena.jpg",
                alt:"Federico Balbuena picture"}
    },
    skills:[
        {iconClassName:"icon fa-gem",
        title:"Quality",
        description:"I assume Code`s Quality as part of my job, and enjoy code reviews, they make me improve my coding, or explain it better, and thanks to that I grow as a developer."},
        {iconClassName:"icon solid fa-paper-plane",
        title:"English advanced",
        description:"I have experience in meetings with North American and Indian development teams, and I`m also certified by the University of Cambridge in English"},
        {iconClassName:"icon solid fa-rocket",
        title:"Experienced",
        description:"I have experience creating code from scratch to generate MVPs and also helped in the maintenance and refactoring of legacy code in large-scale projects."},
        {iconClassName:"icon solid fa-signal",
        title:"Methodologies friendly",
        description:"I find agile methodologies or frameworks like SCRUM, very useful for getting the job done. I understand the need for micro meetings and I always have the will to do it in the best, and quick, possible way."}
      ]
})
const mapDispatchToProps=(dispatch)=>({
    fetchProjects:()=>dispatch(fetchProjects())
})
export default connect(mapStateToProps,mapDispatchToProps)(HomeContainer)